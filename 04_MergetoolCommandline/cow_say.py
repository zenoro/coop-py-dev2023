import cowsay
import shlex
import cmd
import readline

ERROR_MSG = "Some parameters were not set, output with default parameters"

def complete(text, line, begin, end):
    MB_ADDER = {"-b": ["cowsay", "cowthink"],
                "-d": '',
                "-w": ["True", "False"]}
    CS_ADDER = CT_ADDER = {"-e": ["oO", "Oo", "TT", "$$", "@O", "XX", "--", "_-", "X-"],
                            "-c": cowsay.list_cows(),
                            "-T": ["qp",';b' , "U ", "  ", "<>", "^^", "||"]}
    ADDER = {"cowsay": CS_ADDER, "cowthink": CT_ADDER, "make_bubble": MB_ADDER}
    key, command = shlex.split(line)[-1] if begin == end else shlex.split(line)[-2], shlex.split(line)[0]
    return [s for s in ADDER[command][key] if s.startswith(text)]


class CowSaid(cmd.Cmd):
    intro = "Cowsay™ command line (2023)\nCommand line where you can talk with yourself\n"
    prompt = "•>"

    def do_list_cows(self, arg):
        """
        Lists all cow file names in the given directory (default cow list)
        list_cows [dir]
        """
        pth = shlex.split(arg, comments=True)
        pth = pth[0] if pth else cowsay.COW_PEN
        print(cowsay.list_cows(pth))

    def do_make_bubble(self, arg):
        '''
        make_bubble [-d width] [-w wrap_text] text

        Text appears like cow's talk but without cow
        
        text    text in bubble
        -b      Brackets of message (cowsay/cowthink ONLY)
        -d      The width of the text bubble (default: 40)
        -w      Whether text should be wrapped in the bubble (default: True)
        '''
        defaultdict = {'-b':'cowsay', '-d': 40, '-w': True}
        *opts, msg = shlex.split(arg, comments=True)
        if opts:
            try:
                for i in range(0, len(opts), 2):
                    defaultdict[opts[i]] = opts[i+1]
                if defaultdict["-w"] in ['false', 'False', 'f', 'F', 'FALSE']:
                    defaultdict["-w"] = False
            except IndexError:
                print(ERROR_MSG)
        print(cowsay.make_bubble(msg,
                                brackets=cowsay.THOUGHT_OPTIONS[defaultdict["-b"]],
                                width=int(defaultdict["-d"]),
                                wrap_text= bool(defaultdict["-w"])))


    def complete_make_bubble(self, text, line, begidx, endidx):
        return complete(text, line, begidx, endidx)

    def do_cowsay(self, arg):
        '''
        cowsay [-e eye_string] [-c cow] [-T tongue_string] message

        Display a message as legendary cow phrases
        
        -e    Selects the appearance of the cow's eyes. It must be 2 characters long.
        -c    A custom string representing a cow
        -T    A custom tongue string. It must be 2 characters long.
        '''
        defaultdict = {'-e': cowsay.Option.eyes, '-c': 'default', '-T': cowsay.Option.tongue}

        *opts, msg = shlex.split(arg, comments=True)
        if opts:
            try:
                for i in range(0, len(opts), 2):
                    defaultdict[opts[i]] = opts[i+1]
            except IndexError:
                print(ERROR_MSG)
        print(cowsay.cowsay(msg,
                            cow=defaultdict['-c'],
                            eyes=defaultdict['-e'][:2],
                            tongue=defaultdict['-T'][:2]))

    def complete_cowsay(self, text, line, begidx, endidx):
        return complete(text, line, begidx, endidx)

    def do_cowthink(self, arg):
        '''
        cowthink  [-e eye_string] [-c cow] [-T tongue_string] message
        
        Display a message as legendary cow thought
        -e    Selects the appearance of the cow's eyes. It must be 2 characters long.
        -c    A custom string representing a cow
        -T    A custom tongue string. It must be 2 characters long.
        '''
        defaultdict = {'-e': cowsay.Option.eyes, '-c': 'default', '-T': cowsay.Option.tongue}
        *opts, msg = shlex.split(arg, comments=True)
        if opts:
            try:
                for i in range(0, len(opts), 2):
                    defaultdict[opts[i]] = opts[i+1]
            except IndexError:
                print(ERROR_MSG)

        print(cowsay.cowthink(msg,
                            cow=defaultdict['-c'],
                            eyes=defaultdict['-e'][:2],
                            tongue=defaultdict['-T'][:2]))

    def complete_cowthink(self, text, line, begidx, endidx):
        return complete(text, line, begidx, endidx)

    def do_exit(self, arg):
        'Ends command line'
        print("Moobye!")
        return 1


if __name__ == "__main__":
    CowSaid().cmdloop()
