import random as rm
import argparse
import urllib.request as req
from os import getcwd



def bullscows(guess: str, secret: str) -> tuple[int]:
    """
    возвращает количество «быков» и «коров» из guess в secret\n
        «быки» — это одинаковые буквы, которые в словах стоят в одинаковых местах, «коровы» — сколько букв догадки использовано в загадке\n
            Например, bullscows("ропот", "полип") -> (1, 2) («о» — бык, «о» и «п» — коровы)
    """
    bull, cow = 0,0
    for ind, char in enumerate(guess):
        if char == secret[ind]:
            bull += 1
        elif char in secret:
            cow += 1
    return (bull, cow)


def gameplay(ask: callable, inform: callable, words: list[str]) -> int:
    """
    функция-приложение, обеспечивающая геймплей:\n
        0) Задумывает случайное слово из списка слов words: list[str]\n
        1) Спрашивает у пользователя слово с помощью функции ask("Введите слово: ", words)\n
        2) Выводит пользователю результат с помощью функции inform("Быки: {}, Коровы: {}", b, c)\n
        3) Если слово не отгадано, переходит к п. 1\n
        4) Если слово отгадано, возвращает количество попыток — вызовов ask()
    """
    cnt = 1
    resword = rm.choice(words)
    tryword = ask("Введите слово: ", words)
    while tryword.lower().strip() != resword.lower().strip():
        cnt += 1
        inform("Bulls: {}, Cows: {}", *bullscows(tryword, resword))
        tryword = ask("Введите слово: ", words)
    else:
        return cnt


def ask(prompt: str, valid: list[str] = None) -> str:
    """
    Если необязательный параметр valid не пуст, допустим только ввод слова из valid, иначе спрашивает повторно\n
    реализация - обычный input() (если нужно, в цикле с проверкой валидности)
    """
    tryword = input(prompt)
    while valid and tryword not in valid:
        tryword = input(prompt)
    return tryword


def inform(format_string: str, bulls: int, cows: int) -> None:
    """
    реализация - обычный print()
    """
    print(format_string.format(bulls, cows))





parser = argparse.ArgumentParser()
parser.add_argument("words", help='Words dictionary path (URL or local file with absolute path)', default='https://gitlab.com/zenoro/coop-py-dev2023/03_MergeRequirements/defaultdict.txt', type=str)
parser.add_argument("length", help='Max length of words in your dictionary', nargs="?", default=5, type=int)
args = parser.parse_args()


"""
Download words dictionary
"""
if args.words.startswith("https://") or args.words.startswith("http://") or args.words.startswith("ftp://"):
    file = req.urlretrieve(args.words)[0]
    with open(file, "r") as fd:
        dictwords = [line.strip() for line in fd if line and len(line.strip()) == args.length]
else:
    bufmsg = args.words
    for _ in range(2):
        try:
            with open(bufmsg, "r") as fd:
                dictwords = [line.strip() for line in fd if line and len(line.strip()) == args.length]
            break
        except FileNotFoundError:
            bufmsg = getcwd() + bufmsg.strip()
            continue
    else:
        quit("Wrong Path of words dictionary. Try URL address or local file")


# main        
print(gameplay(ask, inform, dictwords))
