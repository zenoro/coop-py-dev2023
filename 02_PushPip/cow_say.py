import cowsay as cs
import argparse
from os import getcwd

"""
Adding arguments to "help" message with all parameters 
"""

parser = argparse.ArgumentParser(description="Cows can tell you something", epilog="It looks similar with something, doesn\'t it?")
parser.add_argument('message', help='A string to wrap in the text bubble.', default=" ", nargs="?")
parser.add_argument('-b', dest='b', help='Invokes Borg mode.', action='store_true')
parser.add_argument('-d', dest='d', help='Causes the cow to appear dead.', action='store_true')
parser.add_argument('-g', dest='g', help='Invokes greedy mode.', action='store_true')
parser.add_argument('-p', dest='p', help='Causes a state of paranoia to come over the cow.', action='store_true')
parser.add_argument('-s', dest='s', help='Makes the cow appear thoroughly stoned.', action='store_true')
parser.add_argument('-t', dest='t', help='Yields a tired cow.', action='store_true')
parser.add_argument('-w', dest='w', help='Initiates wired mode.', action='store_true')
parser.add_argument('-y', dest='y', help='Brings on the cow\'s youthful appearance.', action='store_true')
parser.add_argument('-e', dest='eyes', default='OO', help='Selects the appearance of the cow\'s eyes. It must be 2 characters long.', type=str)
parser.add_argument('-f', dest='cowfile', default='default', help='A custom string representing a cow', type=str)
parser.add_argument('-l', help='Lists the defined cows on the current COWPATH.', action='store_true')
parser.add_argument('-n', dest='wrap', help='Whether text should be wrapped in the bubble.', action='store_true')
parser.add_argument('-T', dest='tongue', help='A custom tongue string. It must be 2 characters long.', default="  ", action='store')
parser.add_argument('-W', dest='width', help='The width of the text bubble.', default=40, type=int)

args = parser.parse_args()

# check if cow-list needed
if args.l:
	print(*cs.list_cows())

else:
    # prepare message - is it file or not
    if '/' in args.message:
        bufmsg = args.message
        for _ in range(2):
            try:
                with open(bufmsg, "r") as msg:
                    txt = ""
                    for line in msg:
                        txt += line
                args.message = txt
                break
            except FileNotFoundError:
                bufmsg = getcwd() + bufmsg.strip()
                continue

    # prepare cowfiles
    if '/' in args.cowfile:
        cow, cowfile = 'default', args.cowfile
    else:
        cow, cowfile = args.cowfile, None

    # create list of cows parameters, take the last one if it not empty
    inpreset = [elem for elem in "bdgpstwy" if args.__dict__[elem]]

    print(cs.cowsay(
        message=args.message,
        cow=cow,
        preset=inpreset[-1] if inpreset else None,
        eyes=args.__dict__['eyes'][:2],
        tongue=args.__dict__['tongue'][:2],
        width=args.__dict__['width'],
        wrap_text=args.wrap,
        cowfile=cowfile
    ))
