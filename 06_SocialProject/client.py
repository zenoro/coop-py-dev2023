import shlex
import cowsay
import cmd
import sys
import threading
import readline
import shlex
import socket

class Simple(cmd.Cmd):
    intro = 'CowNet ver. 1.0\nDo not pass confidential information to cows'
    prompt = '•> '

    def __init__(self, *args, **params):
        super().__init__(*args, **params)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(("localhost", 1337))

    def do_login(self, arg):
        arg = shlex.split(arg)
        if len(arg) != 1:
            print('Wrong name: use login from list_cows()\n')
        else:
            self.socket.send(f'login {arg[0]}\n'.encode())
    
    def do_cows(self, arg=''):
        self.socket.send(f'cows\n'.encode())

    def do_say(self, arg):
        arg = shlex.split(arg)
        if len(arg) == 2:
            self.socket.send(f'say {" ".join(arg)}\n'.encode())
        else:
            print("Wrong message format: try \nsay <destination_login> 'message'\n")
    
    def do_who(self, arg=''):
        self.socket.send(f'who\n'.encode())
    
    def do_yield(self, arg):
        arg = shlex.split(arg)
        if len(arg) == 1:
            self.socket.send(f'yield {arg[0]}\n'.encode())
        else:
            print('Wrong yield usage: there is only one argument\n')

    def do_quit(self, arg=''):
        self.socket.send('quit\n'.encode())
        sys.exit()

    def receive(self):
        while True:
            response = self.socket.recv(1024).decode().strip()
            if not response:
                break
            else:
                print(f"\n{response}\n{readline.get_line_buffer()}", end="", flush=True)


cmdline = Simple()
timer = threading.Thread(target=cmdline.receive, args=())
timer.start()
cmdline.cmdloop()
