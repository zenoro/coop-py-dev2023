import asyncio
import cowsay as cs
from shlex import split as shsplit


def protego(s:list, i:int) -> str:
    """
    Function-exception to avoid empty buffers
    """
    try:
        res = " ".join(s[i:])
    except:
        res = ' '
    return res


usrlst = lambda lst, i: [lst[usr][i] for usr in lst]

clients = {}
users = {}


async def chat(reader, writer):
    me = "{}:{}".format(*writer.get_extra_info('peername'))
    users[me] = asyncio.Queue()
    send = asyncio.create_task(reader.readline())
    receive = asyncio.create_task(users[me].get())

    while not reader.at_eof():
        done, _ = await asyncio.wait([send, receive], return_when=asyncio.FIRST_COMPLETED)
        for q in done:
            if q is send:
                send = asyncio.create_task(reader.readline())
                txt = q.result().decode().strip()
                command = shsplit(txt)
                if command:

                    if command[0] == 'login':
                        if me in clients:
                            await users[me].put(f">> You've already logged in!")
                        else:
                            if command[1] in cs.list_cows():
                                if command[1] not in usrlst(clients, 0):
                                    clients[me] = command[1], me
                                    print(f"<{command[1]}> CONNECTED")
                                    await users[me].put(f">> Logged in as <{command[1]}>")
                                else:
                                    await users[me].put(f">> Log in Error: the nickname has been chosen. Try another nickname from cowsay.list_cows")
                            else:
                                await users[me].put(f">> Log in Error: bad name. Try another nickname from cowsay.list_cows")
                    
                    else:
                        if not clients.get(me,0):
                            await users[me].put(f">> Log in Error: You haven't logged in chat yet")
                            break
                        else:
                    
                            if command[0] == 'who':
                                allguys = ", ".join(usrlst(clients,0))
                                await users[me].put(f">> All users: {allguys}")

                            elif command[0] == 'cows':
                                res = ">> Free nicknames: " + ", ".join(set(cs.list_cows()) - set(usrlst(clients,0)))
                                await users[me].put(res)

                            elif command[0] == 'say':
                                if command[1] in usrlst(clients, 0):
                                    for out in clients.values():
                                        if out[0] == command[1]:
                                            res = cs.cowsay(protego(command,2)) + '\n'
                                            await users[out[1]].put(f">> Send message from {clients[me][0]}:\n{res}")
                                            break
                                else:
                                    await users[me].put(f">> There's no client with <{command[1]}> name")

                            elif command[0] == 'yield':
                                res = f'>>{clients[me][0]} (everyone):\n' + cs.cowsay(protego(command, 1)) + '\n'
                                for out in clients.values():
                                    if users[out[1]] is not users[me]:
                                        await users[out[1]].put(res)
            
                            elif command[0] == 'quit':
                                send.cancel()
                                receive.cancel()
                                print(clients[me][0], "DISCONNECTED")
                                del clients[me]
                                del users[me]
                                writer.close()
                                await writer.wait_closed()
                                break

                            else:
                                buf = ["Wrong command. Try one of these:", 
                                       "who \t View registered users", "cows \t View free cow names",
                                       "yield <message> \t Send a message to all registered users",
                                       "say <nickname> <message> \t Write a private message to client with nickname",
                                       "quit \t Disconnect"]
                                await users[me].put("\n".join(buf))
                                
            elif q is receive:
                receive = asyncio.create_task(users[me].get())
                writer.write(f"{q.result()}\n".encode())
                await writer.drain()
    send.cancel()
    receive.cancel()
    try:
        del users[me]
        print(clients[me][0], "FORCED DISCONNECTED")
        del clients[me]
    except KeyError:
        pass
    writer.close()
    await writer.wait_closed()


async def main():
    server = await asyncio.start_server(chat, '0.0.0.0', 1337)
    async with server:
        await server.serve_forever()

asyncio.run(main())
